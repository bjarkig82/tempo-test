# Readme


## The aproach

- Created a rails api app with swagger doccumentation, including four endpoints:
  - GET role-for-membership
    -  Returns roles for a given user and team
    -  Returns a Developer role if no role has been assigned
  -  GET membership-for-role
     -  Returns all memberships for a given role
     -  If The default role is provided, all users in each team that do not have other roles assigned are returned
  -  POST role
     - Creates a new role
     - duplicates not allowed
  - POST assign-role
    - Assignes a role to a team member
    - The user must be a part of the team in order to assign a role to him
- Two models were created
  - role, handles the role information
  - user_team_role, handles the relation between users, teams and roles

## How to run

1. get the source `git clone --recursive https://bjarkig82@bitbucket.org/bjarkig82/tempo-test.git`
2. Navigate to the source directory
3. Build the application by running docker-compose build
4. If running on linux, the root user might be the ovner of the files
    a. Can be fixed by running `chown -R $USER:$USER .` in the source directroy
5. Create and migrate the database by running `docker-compose run web rails db:create db:migrate`
6. To run the test, run `docker-compose run web rails test`
7. To start the application run `docker-compose up`
8. Navigate to http://localhost:3000/docs to read the swagger doccumentation