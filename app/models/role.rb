class Role < ApplicationRecord
  has_many :user_team_roles

  validates :name, uniqueness: true
  validates :name, presence: true

  swagger_schema :Role do
    key :required, [:id, :name]
    property :id do
      key :type, :integer
      key :format, :int64
    end
    property :name do
      key :type, :string
    end
  end

  def memberships_for_role
    if (default_role?)
      memberships_for_default_role
    else
      memberships_for_non_default_role
    end
  end


  private

    def default_role?
      name == "Developer"
    end

    def memberships_for_default_role
      users = resp2obj TempoTest.users
      teams = resp2obj TempoTest.teams

      memberships = []

      teams.each do |t|
        other_users = UserTeamRole.where('role_id != ? AND team_id = ?', id, t['id']).map(&:user_id).uniq
        team = resp2obj TempoTest.team t['id']

        if team['members']
          memberships << { 
            team_id: t['id'],
            members: team['members'] - other_users
          }
        end
      end

      memberships
    end

    def memberships_for_non_default_role
      assigned_roles = UserTeamRole.where(role_id: id)

      memberships = []

      assigned_roles.group_by(&:team_id).each do |team|
        memberships << {
          team_id: team[0],
          members: team[1].map(&:user_id)
        }
      end

      memberships
    end
end
