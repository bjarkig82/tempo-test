class ApplicationRecord < ActiveRecord::Base
  include Swagger::Blocks
  self.abstract_class = true

  private

    def resp2obj resp
      if resp.code >= 200 and resp.code < 300
        return JSON.parse resp.body
      end

      nil
    end
end
