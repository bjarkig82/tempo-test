class UserTeamRole < ApplicationRecord
  belongs_to :role

  validates :user_id, uniqueness: { scope: [:team_id, :role_id] }

  validate :validate_user, :validate_team, :validate_user_in_team

  def user
    resp2obj TempoTest.user(user_id)
  end

  def team
    resp2obj TempoTest.team(team_id)
  end

  private

    def validate_user
      users = resp2obj TempoTest.users
      user_ids = users.map { |user| user['id'] }

      unless user_ids.include? user_id
        errors.add(:user_id, 'User does not exist')
      end
    end

    def validate_team
      teams = resp2obj TempoTest.teams
      team_ids = teams.map { |team| team['id'] }

      unless team_ids.include? team_id
        errors.add(:team_id, 'Team does not exist')
      end
    end

    def validate_user_in_team
      unless team and team['members'] and team['members'].include? user_id
        errors.add(:team_id, 'User not in team')
      end
    end
end
