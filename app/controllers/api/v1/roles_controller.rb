module Api::V1
  class RolesController < ApplicationController

    swagger_path '/role-for-membership' do
      operation :get do
        key :summary, 'Find user role in a team'
        key :description, 'Returns the user role in the corresponding team'
        key :operationId, 'findRoleForMembership'
        key :tags, [
          'role'
        ]
        parameter do
          key :name, :user_id
          key :in, :query
          key :description, 'ID of the user'
          key :required, true
          key :type, :integer
          key :format, :int64
        end
        parameter do
          key :name, :team_id
          key :in, :query
          key :description, 'ID of the team'
          key :required, false
          key :type, :integer
          key :format, :int64
        end
        response 200 do
          key :description, 'Role for membership'
        end
        response :default do
          key :description, 'Error'
        end
      end
    end

    def role_for_membership
      if params[:user_id] and params[:team_id]
        user_team_role = UserTeamRole.find_by(user_id: params[:user_id], team_id: params[:team_id])
      
        if user_team_role and user_team_role.role
          render json: user_team_role.role, status: :ok
        else
          render status: :not_found, json: { error: 'Role not found' }
        end
      else
        render status: :bad_request, json: { error: 'user_id and team_id must be set' }
      end
    end

    swagger_path '/memberships-for-role' do
      operation :get do
        key :summary, 'Find all memberships for a role'
        key :description, 'Returns the memberships for the corresponding role'
        key :operationId, 'findMembershipForRole'
        key :tags, [
          'role'
        ]
        parameter do
          key :name, :role_id
          key :in, :query
          key :description, 'ID of the role'
          key :required, true
          key :type, :integer
          key :format, :int64
        end
        response 200 do
          key :description, 'Memberships for role'
        end
        response :default do
          key :description, 'error'
        end
      end
    end

    def memberships_for_role
      if params[:role_id]
        role = Role.find_by(id: params[:role_id])
      
        if role
          render json: role.memberships_for_role, status: :ok
        else
          render status: :not_found, json: { error: 'Role not found' }
        end
      else
        render status: :bad_request, json: { error: 'role_id must be set' }
      end
    end

    swagger_path '/assign-role' do
      operation :post do
        key :summary, 'Assign a role to a membership'
        key :description, 'Returns the memberships for the corresponding role'
        key :operationId, 'assignRole'
        key :tags, [
          'role'
        ]
        parameter do
          key :name, :user_id
          key :in, :query
          key :description, 'ID of the user'
          key :required, true
          key :type, :integer
          key :format, :int64
        end
        parameter do
          key :name, :team_id
          key :in, :query
          key :description, 'ID of the team'
          key :required, true
          key :type, :integer
          key :format, :int64
        end
        parameter do
          key :name, :role_id
          key :in, :query
          key :description, 'ID of the role'
          key :required, true
          key :type, :integer
          key :format, :int64
        end
        response 200 do
          key :description, 'User Team Role'
        end
        response :default do
          key :description, 'Error'
        end
      end
    end

    def assign_role
      if params[:user_id] and params[:team_id] and params[:role_id]
        user_team_role = UserTeamRole.new(user_id: params[:user_id], team_id: params[:team_id], role_id: params[:role_id])
      
        if user_team_role.save
          render json: user_team_role, status: :ok
        else
          render status: :unprocessable_entity, json: user_team_role.errors
        end
      else
        render status: :bad_request, json: { error: 'user_id, team_id and role_id must be set' }
      end
    end

    swagger_path '/role' do
      operation :post do
        key :summary, 'Create a role'
        key :description, 'Creates a role'
        key :operationId, 'createRole'
        key :tags, [
          'role'
        ]
        parameter do
          key :name, :name
          key :in, :form
          key :description, 'Name of the role'
          key :required, true
          key :type, :string
          key :format, :int64
        end
        response 200 do
          key :description, 'Role'
        end
        response :default do
          key :description, 'unexpected error'
        end
      end
    end

    def create
      role = Role.new(name: params[:name])
  
      if role.save
        render json: role, status: :ok
      else
        render status: :unprocessable_entity, json: role.errors
      end
    end
    
  end
end
