class TempoTest
  include HTTParty
  base_uri 'http://tempo-test.herokuapp.com/7d1d085e-dbee-4483-aa29-ca033ccae1e4/1'

  def self.users
    get('/user')
  end

  def self.teams
    get('/team')
  end

  def self.team id
    get("/team/#{id}")
  end

  def self.user id
    get("/user/#{id}")
  end
end