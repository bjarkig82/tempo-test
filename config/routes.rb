Rails.application.routes.draw do
  scope module: 'api' do
    namespace :v1 do
        get 'role-for-membership', to: 'roles#role_for_membership'
        get 'memberships-for-role', to: 'roles#memberships_for_role'
        post 'assign-role', to: 'roles#assign_role'
        post 'role', to: 'roles#create'
    end
  end

  resources :apidocs, only: [:index]
  get '/docs' => redirect('/swagger/dist/index.html?url=/apidocs')

end
