ENV['RAILS_ENV'] ||= 'test'
require_relative '../config/environment'
require 'rails/test_help'
require 'webmock/test_unit'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  headers = {
    'Accept'=>'*/*',
    'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
    'User-Agent'=>'Ruby'
  }
  WebMock.stub_request(:get, "http://tempo-test.herokuapp.com/7d1d085e-dbee-4483-aa29-ca033ccae1e4/1/user")
    .with(headers: headers)
    .to_return(status: 200, body: File.read(Rails.root.join('test', 'fixtures', 'users.json')), headers: {})

  WebMock.stub_request(:get, "http://tempo-test.herokuapp.com/7d1d085e-dbee-4483-aa29-ca033ccae1e4/1/team")
    .with(headers: headers)
    .to_return(status: 200, body: File.read(Rails.root.join('test', 'fixtures', 'teams.json')), headers: {})

  (1..3).each do |i|
    WebMock.stub_request(:get, "http://tempo-test.herokuapp.com/7d1d085e-dbee-4483-aa29-ca033ccae1e4/1/team/#{i}")
      .with(headers: headers)
      .to_return(status: 200, body: File.read(Rails.root.join('test', 'fixtures', "team_#{i}.json")), headers: {})
  end

  (1..4).each do |i|
    WebMock.stub_request(:get, "http://tempo-test.herokuapp.com/7d1d085e-dbee-4483-aa29-ca033ccae1e4/1/user/#{i}")
      .with(headers: headers)
      .to_return(status: 200, body: File.read(Rails.root.join('test', 'fixtures', "user_#{i}.json")), headers: {})
  end

  WebMock.stub_request(:get, "http://tempo-test.herokuapp.com/7d1d085e-dbee-4483-aa29-ca033ccae1e4/1/team/1000").
  with(
    headers: {
	  'Accept'=>'*/*',
	  'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
	  'User-Agent'=>'Ruby'
    }).
  to_return(status: 404, body: "", headers: {})

end
