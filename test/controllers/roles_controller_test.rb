require 'test_helper'

class RolesControllerTest < ActionDispatch::IntegrationTest
  test "should get role-for-membership if user_id and team_id are present and user and team exists" do
    get v1_role_for_membership_url, params: { user_id: 1, team_id: 2 }

    assert_response :success
    assert_equal UserTeamRole.find(1).role.to_json, @response.body
  end

  test "role-for-membership should render not found error user_id or team_id does not exist" do
    get v1_role_for_membership_url, params: { user_id: 1, team_id: 100 }
    assert_response :not_found
  end

  test "role-for-membership should render not bad request if user_id or team_id params are missing" do
    get v1_role_for_membership_url, params: { user_id: 1}
    assert_response :bad_request

    get v1_role_for_membership_url, params: { team_id: 1}
    assert_response :bad_request

    get v1_role_for_membership_url
    assert_response :bad_request
  end

  test "memberships-for-role should render memberships information if role exists" do
    get v1_memberships_for_role_url, params: { role_id: 1 }

    assert_response :success
    assert_equal [
      {team_id: 1, members: [4]},
      {team_id: 2, members: [1]},
      {team_id: 3, members: [1,2,3]},
    ].to_json, @response.body

    get v1_memberships_for_role_url, params: { role_id: 2 }
    assert_response :success
    assert_equal [
      {team_id: 1, members: [2]},
      {team_id: 2, members: [3]},
    ].to_json, @response.body

    get v1_memberships_for_role_url, params: { role_id: 3 }
    assert_response :success
    assert_equal [
      {team_id: 1, members: [3]},
    ].to_json, @response.body
  end

  test "memberships-for-role should render not found if no role is found" do
    get v1_memberships_for_role_url, params: { role_id: 1000 }
    assert_response :not_found
  end

  test "memberships-for-role should render not bad request if the role_id param is missing" do
    get v1_memberships_for_role_url
    assert_response :bad_request
  end

  test "should only be able to assign role to an existing membership" do
    post v1_assign_role_url, params: {user_id: 1000, team_id: 1, role_id: 2}
    assert_response :unprocessable_entity

    post v1_assign_role_url, params: {user_id: 1, team_id: 1000, role_id: 2}
    assert_response :unprocessable_entity

    post v1_assign_role_url, params: {user_id: 4, team_id: 1, role_id: 1000}
    assert_response :unprocessable_entity

    post v1_assign_role_url, params: {user_id: 1, team_id: 1, role_id: 1}
    assert_response :unprocessable_entity

    post v1_assign_role_url, params: {user_id: 4, team_id: 1, role_id: 2}
    assert_response :success
  end

  test "should render bad_request if parameter is missing while assigning role" do
    post v1_assign_role_url, params: {team_id: 1, role_id: 2}
    assert_response :bad_request

    post v1_assign_role_url, params: {user_id: 1, role_id: 2}
    assert_response :bad_request

    post v1_assign_role_url, params: {user_id: 4, team_id: 1}
    assert_response :bad_request

  end

  test "should be able to create role if name is present and unique" do
    post v1_role_url
    assert_response :unprocessable_entity

    post v1_role_url params: {name: "Developer"}
    assert_response :unprocessable_entity

    post v1_role_url params: {name: "New Role"}
    assert_response :success
  end
end
