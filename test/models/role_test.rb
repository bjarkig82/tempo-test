require 'test_helper'

class RoleTest < ActiveSupport::TestCase
  test "Should not be able to register a role that already exists" do
    role = Role.new(name: 'Developer')
    assert_not role.save
  end
end
