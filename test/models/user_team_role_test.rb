require 'test_helper'

class UserTeamRoleTest < ActiveSupport::TestCase
  test "User should only be able to have a single role within a team" do
    utr = UserTeamRole.new(user_id: 3, team_id: 3, role_id: 3)
    assert utr.save

    utr = UserTeamRole.new(user_id: 1, team_id: 2, role_id: 1)
    assert_not utr.save
  end

  test "Should not be able to assign a role if either user, team or role does not exist" do
    utr = UserTeamRole.new(user_id: 1000, team_id: 3, role_id: 3)
    assert_not utr.save

    utr = UserTeamRole.new(user_id: 1, team_id: 1000, role_id: 1)
    assert_not utr.save

    utr = UserTeamRole.new(user_id: 1, team_id: 1, role_id: 1000)
    assert_not utr.save
  end
end
