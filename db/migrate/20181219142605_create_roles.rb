class CreateRoles < ActiveRecord::Migration[5.2]
  def change
    create_table :roles do |t|
      t.string :name
      t.timestamps
    end

    roles = ["Developer", "Product Owner", "Tester"]

    roles.each do |role|
      Role.create name: role
    end
  end
end
