class CreateUserTeamRoles < ActiveRecord::Migration[5.2]
  def change
    create_table :user_team_roles do |t|
      t.references :user
      t.references :team
      t.references :role
      t.timestamps
    end
  end
end
